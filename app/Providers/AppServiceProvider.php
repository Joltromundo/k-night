<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(
            'App\Repositories\Contracts\UserRepositoryInterface',
            'App\Repositories\Eloquent\UserRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\CampaignRepositoryInterface',
            'App\Repositories\Eloquent\CampaignRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\CharRepositoryInterface',
            'App\Repositories\Eloquent\CharRepository'
        );
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
