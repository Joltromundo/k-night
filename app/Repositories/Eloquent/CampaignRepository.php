<?php

namespace App\Repositories\Eloquent;

use App\Models\Campaign;
use App\Repositories\Contracts\CampaignRepositoryInterface;

class CampaignRepository extends AbstractRepository implements CampaignRepositoryInterface
{
    protected $model = Campaign::class;

    public function allByUser(string $id)
    {
        return $this->model->where('user_id', $id)->get();
    }
}