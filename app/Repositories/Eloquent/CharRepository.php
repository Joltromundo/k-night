<?php

namespace App\Repositories\Eloquent;

use App\Models\Char;
use App\Repositories\Contracts\CharRepositoryInterface;

class CharRepository extends AbstractRepository implements CharRepositoryInterface
{
    protected $model = Char::class;
}