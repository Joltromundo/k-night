<?php

namespace App\Repositories\Contracts;

interface CampaignRepositoryInterface
{
    public function all();
    public function allByUser(string $id);
    public function create($request);
    public function find(string $id);
    public function update($request, string $id);
    public function delete(string $id);
}
