<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Services\UserService;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;

class RegisterController extends Controller
{

    public function __construct(
        protected UserService $service
    ){}

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('Register/Index', [
            'message' => "Register"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUserRequest $request, UserService $service): RedirectResponse
    {
        $service->create($request->all());
        
        return redirect(route('login'))->with([
            'success' => 'User was successfully created!'
        ]);
    }

}
