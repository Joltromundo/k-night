<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCampaignRequest;
use App\Services\CampaignService;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class CampaignController extends Controller
{
    public function __construct(
        protected CampaignService $service
    ){}
    /**
     * Display a listing of the resource.
     */
    public function index(CampaignService $service)
    {
        $campaigns = $service->allByUser(Auth::user()->id);

        return Inertia::render('Campaign/Index', [
            'title' => 'Campaigns',
            'create_url' => 'campaigns.create',
            'user' => Auth::user(),
            'campaigns' => $campaigns
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Campaign/Create', [
            'title' => 'Campaigns',
            'user' => Auth::user()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCampaignRequest $request, CampaignService $service)
    {   
        $data = [
            'user_id' => Auth::user()->id,
            'name' => $request['name'],
            'description' => $request['description'],
            'image' => $request['image'],
            'category' => $request['category'],
        ];

        $service->create($data);

        return redirect(route('campaigns.index'))->with([
            'success' => 'Campaign created successfully'
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(CampaignService $service, string $id)
    {
        $campaign = $service->find($id);

        if($campaign->user_id == Auth::user()->id){
            return Inertia::render('Campaign/Show', [
                'title' => 'Campaigns',
                'create_url' => 'campaigns.create',
                'user' => Auth::user(),
                'campaign' => $campaign
            ]);
        } else {
            return to_route('campaigns.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(CampaignService $service, string $id)
    {
        $campaign = $service->find($id);

        if($campaign->user_id == Auth::user()->id){
            return Inertia::render('Campaign/Create', [
                'title' => 'Campaigns',
                'user' => Auth::user(),
                'campaign' => $campaign
            ]);
        } else {
            return to_route('campaigns.index');
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CampaignService $service, StoreCampaignRequest $request, string $id)
    {
        $campaign = $service->find($id);

        if($campaign->user_id == Auth::user()->id){
            $data = [
                'user_id' => Auth::user()->id,
                'name' => $request['name'],
                'description' => $request['description'],
                'image' => $request['image'],
                'category' => $request['category'],
            ];

            $service->update($data, $id);

            return redirect(route('campaigns.show', $campaign->id))->with([
                'success' => 'Campaign updated successfully'
            ]);
        } else {
            return response("Not Allowed");
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(CampaignService $service, string $id)
    {
        $campaign = $service->find($id);

        if($campaign->user_id == Auth::user()->id){
            $service->delete($id);

            return redirect(route('campaigns.index'))->with([
                'success' => 'Campaign deleted successfully'
            ]);
        } else {
            return response("Not Allowed");
        }
    }
}
