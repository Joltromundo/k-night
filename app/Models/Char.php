<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Char extends Model
{   
    use SoftDeletes;
    use HasFactory;

    protected $fillable = [
        'user_id',
        'campaign_id',
        'name',
        'description',
        'photo',
        'type',
        'sheet'
    ];

    protected $dates = [
        'deleted_at'
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function campaigns(): MorphToMany
    {
        return $this->morphToMany(Campaign::class, 'taggable');
    }
}
