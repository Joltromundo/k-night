<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Campaign extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $fillable = [
        'user_id',
        'name',
        'description',
        'image',
        'category'
    ];

    protected $dates = [
        'deleted_at'
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function chars(): MorphToMany
    {
        return $this->morphedByMany(Char::class, 'taggable');
    }
}
