<?php

namespace App\Services;

use App\Repositories\Contracts\CampaignRepositoryInterface;

class CampaignService
{
    public function __construct(
        protected CampaignRepositoryInterface $repository
    )
    {}

    public function all()
    {
        return $this->repository->all();
    }

    public function allByUser($id)
    {
        return $this->repository->allByUser($id);
    }

    public function create($request)
    {
        return $this->repository->create($request);
    }

    public function find($id)
    {
        return $this->repository->find($id);
    }

    public function update($request, $id)
    {
        return $this->repository->find($id)->update($request);
    }

    public function delete($id)
    {
        return $this->repository->find($id)->delete();
    }
}